const prizeModel = require('../models/prizeModel');

const mongoose = require ('mongoose');

//createPrize
const createPrize = (req, res) => {
    let body = req.body;

    let createPrize = {
        _id: mongoose.Types.ObjectId(),
        name: body.name,
        description: body.description,

    }

    prizeModel.create(createPrize, (err, prize) => {
        if(err){
            return res.status(500).json({
                message: err
            })
        } else {
            return res.status(200).json({
                message: 'Prize created succesfully!',
                prize: prize
            });
        }
    })
}

const getAllPrizes = (req, res) => {
    prizeModel.find({}, (err, prizes) => {
        if(err){
            return res.status(500).json({
                message: err
            })
        } else {
            return res.status(200).json({
                message: 'Prizes retrieved succesfully!',
                prizes: prizes
            });
        }
    })
}

const getPrizeById = (req, res) => {
    let id = req.params.id;

    prizeModel.findById(id, (err, prize) => {
        if(err){
            return res.status(500).json({
                message: err
            })
        } else {
            return res.status(200).json({
                message: 'Prize retrieved succesfully!',
                prize: prize
            });
        }
    })
}

const updatePrize = (req, res) => {
    let id = req.params.id;
    let body = req.body;

    let updatePrize = {
        name: body.name,
        description: body.description,
    }
    
    prizeModel.findByIdAndUpdate(id, updatePrize, (err, prize) => {
        if(err){
            return res.status(500).json({
                message: err
            })
        } else {
            return res.status(200).json({
                message: 'Prize updated succesfully!',
                prize: prize
            });
        }
    }
    )
}

const deletePrize = (req, res) => {
    let id = req.params.id;

    prizeModel.findByIdAndDelete(id, (err, prize) => {
        if(err){
            return res.status(500).json({
                message: err
            })
        } else {
            return res.status(200).json({
                message: 'Prize deleted succesfully!',
                prize: prize
            });
        }
    }
    )
}

module.exports = { createPrize, getAllPrizes, getPrizeById, updatePrize, deletePrize };