const userModel = require('../models/userModel');

const mongoose = require('mongoose');

const createUser = (req, res) => {
    let body = req.body;

    let createUser = {
        _id: mongoose.Types.ObjectId(),
        userName: body.userName,
        firstName: body.firstName,
        lastName: body.lastName

    }

    userModel.create(createUser, (err, user) => {
        if (err) {
            return res.status(500).json({
                message: err
            });
        } else {
            return res.status(200).json({
                message: 'User created successfully',
                user: user
            });
        }
    })
}

const getAllUsers = (req, res) => {
    userModel.find({}, (err, users) => {
        if (err) {
            return res.status(500).json({
                message: err
            });
        } else {
            return res.status(200).json({
                message: 'Get all users successfully',
                users: users
            });
        }
    })
}

const getUserById = (req, res) => {
    let id = req.params.id;

    userModel.findById(id, (err, user) => {
        if (err) {
            return res.status(500).json({
                message: err
            });
        } else {
            return res.status(200).json({
                message: 'User fetched successfully',

                user: user
            });
        }
    })
}

const updateUser = (req, res) => {
    let id = req.params.id;
    let body = req.body;

    userModel.findByIdAndUpdate(id, body, (err, user) => {
        if (err) {
            return res.status(500).json({
                message: err
            });
        } else {
            return res.status(200).json({
                message: 'User updated successfully',
                user: user
            });
        }
    })
}

const deleteUser = (req, res) => {
    let id = req.params.id;

    userModel.findByIdAndRemove(id, (err, user) => {
        if (err) {
            return res.status(500).json({
                message: err
            });
        } else {
            return res.status(200).json({
                message: 'User deleted successfully',
                user: user
            });
        }
    })
}



module.exports = { createUser, getAllUsers, getUserById, updateUser, deleteUser };
