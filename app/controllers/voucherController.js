const voucherModel = require('../models/voucherModel');

const mongoose = require ('mongoose');

const createVoucher = (req, res) => {
    let body = req.body;

    let createVoucher = {
        _id: mongoose.Types.ObjectId(),
        code: body.code,
        discount: body.discount,
        note: body.note
    }

    voucherModel.create(createVoucher, (err, voucher) => {
        if(err){
            return res.status(500).json({
                message: err
            })
        } else {
            return res.status(200).json({
                message: 'Voucher created succesfully!',
                voucher: voucher
            });
        }
    })
}

const getAllVouchers = (req, res) => {
    voucherModel.find({}, (err, voucher) => {
        if(err){
            return res.status(500).json({
                message: err
            })
        } else {
            return res.status(200).json({
                message: 'Vouchers retrieved succesfully!',
                vouchers: voucher
            });
        }
    })
}

const getVoucherById = (req, res) => {
    let id = req.params.id;

    voucherModel.findById(id, (err, voucher) => {
        if(err){
            return res.status(500).json({
                message: err
            })
        } else {
            return res.status(200).json({
                message: 'Voucher retrieved succesfully!',
                voucher: voucher
            });
        }
    })
}

const updateVoucherById = (req, res) => {
    let id = req.params.id;
    let body = req.body;

    voucherModel.findByIdAndUpdate(id, body, (err, voucher) => {
        if(err){
            return res.status(500).json({
                message: err
            })
        } else {
            return res.status(200).json({
                message: 'Voucher updated succesfully!',
                voucher: voucher
            });
        }
    })
}

const deleteVoucherById = (req, res) => {
    let id = req.params.id;

    voucherModel.findByIdAndRemove(id, (err, voucher) => {
        if(err){
            return res.status(500).json({
                message: err
            })
        } else {
            return res.status(200).json({
                message: 'Voucher deleted succesfully!',
                voucher: voucher
            });
        }
    })
}

module.exports = { createVoucher, getAllVouchers, getVoucherById, updateVoucherById, deleteVoucherById };
