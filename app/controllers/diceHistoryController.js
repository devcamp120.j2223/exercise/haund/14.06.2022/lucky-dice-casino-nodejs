const userModel = require("../models/userModel");

const mongoose = require("mongoose");

const createDiceHistory = (req, res) => {
    let userId = req.body.userId;
    let body = req.body;

    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            message: "Invalid user id"
        });
    }

    let createDiceHistory = {
        _id: mongoose.Types.ObjectId(),

        dice: body.dice,
    }
    diceHistoryModel.create(createDiceHistory, (err, diceHistory) => {
        if (err) {
            return res.status(500).json({
                message: err
            });
        } else {
            return res.status(200).json({
                message: 'DiceHistory created successfully',
                diceHistory: diceHistory
            });
        }
    })
}

const getAllDiceHistories = (req, res) => {
    diceHistoryModel.find({}, (err, diceHistories) => {
        if (err) {
            return res.status(500).json({
                message: err
            });
        } else {
            return res.status(200).json({
                message: 'Get all diceHistories successfully',
                diceHistories: diceHistories
            });
        }
    }
    )
}

const getDiceHistoryById = (req, res) => {
    let id = req.params.id;

    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            message: 'Invalid diceHistory id'
        });

    }

    diceHistoryModel.findById(id, (err, diceHistory) => {
        if (err) {
            return res.status(500).json({
                message: err
            });
        } else {
            return res.status(200).json({
                message: 'DiceHistory fetched successfully',
                diceHistory: diceHistory
            });
        }
    }
    )
}

const updateDiceHistory = (req, res) => {
    let id = req.params.id;
    let body = req.body;

    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            message: 'Invalid diceHistory id'
        });

    }
    diceHistoryModel.findByIdAndUpdate(id, body, (err, diceHistory) => {
        if (err) {
            return res.status(500).json({
                message: err
            });
        } else {
            return res.status(200).json({
                message: 'DiceHistory updated successfully',
                diceHistory: diceHistory
            });
        }
    }
    )
}

const deleteDiceHistory = (req, res) => {
    let id = req.params.id;

    if (!mongoose.Types.ObjectId.isValid(id)) {
        return res.status(400).json({
            message: 'Invalid diceHistory id'
        });

    }
    diceHistoryModel.findByIdAndRemove(id, (err, diceHistory) => {
        if (err) {
            return res.status(500).json({
                message: err
            });
        } else {
            return res.status(200).json({
                message: 'DiceHistory deleted successfully',
                diceHistory: diceHistory
            });
        }
    }
    )
}

module.exports = { createDiceHistory, getAllDiceHistories, getDiceHistoryById, updateDiceHistory, deleteDiceHistory };

