const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const prizeSchema = new Schema({
    _id: {
        type: mongoose.Types.ObjectId,
        
        required: true
    },
    name:{
        type: String,
        required: true,
        unique: true
    },
    description:{
        type: String,
        required: false
    },
    createAt:{
        type: Date,
        default: Date.now()
    },
    updateAt:{
        type: Date,
        default: Date.now()
    },
});

module.exports = mongoose.model('Prize', prizeSchema);