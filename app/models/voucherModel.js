const mongoose =  require('mongoose');

const Schema = mongoose.Schema;

const voucherSchema = new Schema({
    _id:{
        type: mongoose.Types.ObjectId,
        
        required: true
    },
    code: {
        type: String,
        required: true,
        unique: true
    },
    discount: {
        type: Number,
        required: true
    },
    note: {
        type: String,
        required: false
    },
    createAt: {
        type: Date,
        default: Date.now()

    },
    updateAt :{
        type: Date,
        default: Date.now()
    },
});

module.exports = mongoose.model('Voucher', voucherSchema);
