const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const diceHistorySchema = new Schema({
    _id: mongoose.Types.ObjectId,
    user: {
        type: mongoose.Types.ObjectId,
        ref: "User",
        required: true
    },
    dice: {
        type: Number,
        required: true
    },
    createAt: {
        type: Date,
        default: Date.now()
    },
    updateAt:{
        type: Date,
        default: Date.now()
    },
});
module.exports = mongoose.model("Dice-History", diceHistorySchema);