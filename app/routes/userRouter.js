const express = require('express');

const { printTime, printRequestMethod } = require('../middlewares/appMiddleware');

const { createUser, getAllUsers, getUserById, updateUser, deleteUser } = require('../controllers/userController');

const router = express.Router();

router.post('/users',printTime, printRequestMethod, createUser);

router.get('/users',printTime, printRequestMethod, getAllUsers);

router.get('/users/:id', printTime, printRequestMethod, getUserById);

router.put('/users/:id',printTime, printRequestMethod, updateUser);

router.delete('/users/:id', printTime, printRequestMethod, deleteUser);

module.exports = router;