const express = require ('express');

const { printTime, printRequestMethod } = require ('../middlewares/appMiddleware');

const { createDiceHistory, getAllDiceHistories, getDiceHistoryById, updateDiceHistory, deleteDiceHistory } = require ('../controllers/diceHistoryController');

const router = express.Router();

router.post('/dice-histories', printTime, printRequestMethod, createDiceHistory);

router.get('dice-histories', printTime, printRequestMethod, getAllDiceHistories);

router.get('/dice-histories/:diceHistoryId', printTime, printRequestMethod, getDiceHistoryById);

router.put('/diceHistories/:diceHistoryId', printTime, printRequestMethod, updateDiceHistory);

router.delete('/diceHistories/:diceHistoryId', printTime, printRequestMethod, deleteDiceHistory);

module.exports = router;