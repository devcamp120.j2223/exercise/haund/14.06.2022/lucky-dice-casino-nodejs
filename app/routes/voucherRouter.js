const express = require ('express');

const { printTime, printRequestMethod } = require ('../middlewares/appMiddleware');

const { createVoucher, getAllVouchers, getVoucherById, updateVoucherById, deleteVoucherById } = require ('../controllers/voucherController');

const router = express.Router();

router.post('/vouchers', printTime, printRequestMethod, createVoucher);

router.get('/vouchers', printTime, printRequestMethod, getAllVouchers);

router.get('/vouchers/:voucherId', printTime, printRequestMethod, getVoucherById);

router.put('/vouchers/:voucherId', printTime, printRequestMethod, updateVoucherById);

router.delete('/vouchers/:voucherId', printTime, printRequestMethod, deleteVoucherById);

module.exports = router;