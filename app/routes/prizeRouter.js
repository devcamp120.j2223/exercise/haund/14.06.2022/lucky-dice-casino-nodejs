const express = require ('express');

const { printTime, printRequestMethod } = require ('../middlewares/appMiddleware');

const { createPrize, getAllPrizes, getPrizeById, updatePrize, deletePrize } = require ('../controllers/prizeController');

const router = express.Router();

router.post('/prizes', printTime, printRequestMethod, createPrize);

router.get('/prizes', printTime, printRequestMethod, getAllPrizes);

router.get('/prizes/:prizeId', printTime, printRequestMethod, getPrizeById);

router.put('/prizes/:prizeId', printTime, printRequestMethod, updatePrize);

router.delete('/prizes/:prizeId', printTime, printRequestMethod, deletePrize);

module.exports = router;