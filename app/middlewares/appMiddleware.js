// viet middleware in ra thoi gian chay cua app
const printTime = (req, res, next) => {
    console.log("App run at:")
    console.log(`${new Date()}`);
    next();
    }
// viet middleware in ra request method moi lan chay
const printRequestMethod = (req, res, next) => {
    console.log("------------------------------")
    console.log(`Request method: ${req.method}`);
    next();
    }

module.exports = { printTime, printRequestMethod };