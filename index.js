const express = require("express");

const app = express();

const path = require('path');

const { printTime, printRequestMethod } = require('./app/middlewares/appMiddleware');
const userRouter = require("./app/routes/userRouter");
const diceHistoryRouter = require("./app/routes/diceHistoryRouter");
const prizeRouter = require('./app/routes/prizeRouter');
const voucherRouter = require('./app/routes/voucherRouter');
const port = 8000;



const mongoose = require("mongoose");
mongoose.connect('mongodb://localhost:27017/Lucky_Dice', (err) => {
    if(err){
        throw err;
    }
    console.log('Connected to MongoDB');
})


app.use(express.json());

app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, 'views/index.html'));
});

 app.use('/', userRouter);
 app.use('/', diceHistoryRouter);
 app.use('/', voucherRouter);
 app.use('/', prizeRouter);
app.get("/", printTime, printRequestMethod, (req, res) => {
    console.log("app đã chạy");
})

app.listen(port, () => console.log(`Application running to port ${port}`))